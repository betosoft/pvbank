//
//  VPLoginVC.m
//  PVBank
//
//  Created by Dao Duy Thuy on 6/11/15.
//  Copyright (c) 2015 ThuyDao. All rights reserved.
//

#import "VPLoginVC.h"

@implementation VPLoginVC

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self td_showNavigationBar:NO];
    [self td_hideBackbutton];
    
    
    //demo loading
//    [TDLoadingView showInView:self.view animate:YES];
//    
//    [self td_delay:5 completed:^{
//        [TDLoadingView hideAllInView:self.view animate:YES];
//    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

#pragma mark - IBAction

#pragma mark - Action

- (void)loginViaFacebook
{
    //todo something
    //call api login
    [self callLogin];
}

#pragma mark - FakeData

#pragma mark - Delegate

#pragma mark - Navigation


@end


//============================================================================================================================================================================================================================================================================================================================================================================================================

@implementation VPLoginVC (API)

- (void)callLogin
{
    //todo something
}

@end