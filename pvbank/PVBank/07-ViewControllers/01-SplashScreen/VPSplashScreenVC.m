//
//  VPSplashScreenVC.m
//  PVBank
//
//  Created by Dao Duy Thuy on 6/11/15.
//  Copyright (c) 2015 ThuyDao. All rights reserved.
//

#import "VPSplashScreenVC.h"

@implementation VPSplashScreenVC

#pragma mark - LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self td_hideNavigationBar:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    __weak typeof(self)weakSelf = self;
     [TDLoadingView showInView:self.view animate:YES];
    [self td_delay:5.0 completed:^{
        [TDLoadingView hideAllInView:weakSelf.view animate:YES];
        [weakSelf gotoLogin];
        
    }];
    
}

#pragma mark - IBAction

#pragma mark - Action

#pragma mark - FakeData

#pragma mark - API

#pragma mark - Delegate

#pragma mark - Navigation

- (void)gotoLogin
{
    [self td_pushViewControllerWithClass:[VPLoginVC class]];
}


@end
