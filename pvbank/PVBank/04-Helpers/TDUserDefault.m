//
//  TBUserDefault.m
//  mPos
//
//  Created by thuydd on 1/29/15.
//  Copyright (c) 2015 SmartOSCVietNam. All rights reserved.
//

#import "TDUserDefault.h"

@implementation TDUserDefault

+ (void)setObject:(id)anObject forKey:(NSString *)aKey
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:anObject];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:data forKey:aKey];
    [ud synchronize];
}

+ (id)objectForKey:(NSString *)aKey
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ( [[ud dictionaryRepresentation].allKeys containsObject:aKey] )
    {
        NSData *data = [NSKeyedUnarchiver unarchiveObjectWithData:[ud objectForKey:aKey]];
        
        return data;
    }
    return nil;
}
+ (void)removeObjectForKey:(NSString *)aKey
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:aKey];
    [ud synchronize];
}

@end

#pragma mark - Application

@implementation TDUserDefault (Application)

//TODO: customize here

@end
