//
//  TBUserDefault.h
//  mPos
//
//  Created by thuydd on 1/29/15.
//  Copyright (c) 2015 SmartOSCVietNam. All rights reserved.
//

#import <Foundation/Foundation.h>

#define userDefaultSet(anObject,aKey) [TBUserDefault setObject:anObject forKey:aKey];
#define userDefaultGet(aKey) [TBUserDefault objectForKey:aKey];
#define userDefaultRemove(aKey) [TBUserDefault removeObjectForKey:aKey];

@interface TDUserDefault: NSObject

+ (void)setObject:(id)anObject forKey:(NSString *)aKey;

+ (id)objectForKey:(NSString *)aKey;

+ (void)removeObjectForKey:(NSString *)aKey;

@end

#pragma mark - Application
@interface TDUserDefault (Application)

//TODO: customize here

@end
