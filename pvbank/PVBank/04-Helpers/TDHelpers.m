//
//  TBHelpers.m
//  mPos
//
//  Created by thuydd on 1/29/15.
//  Copyright (c) 2015 SmartOSC. All rights reserved.
//

#import "TDHelpers.h"

@implementation TDHelpers

+ (instancetype)sharedInstance
{
    static TDHelpers *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TDHelpers alloc] init];
    });
    return sharedInstance;
}

@end
