//
//  NSMutableAttributedString+EWMultiStyle.m
//  mPos
//
//  Created by BunLV on 6/5/15.
//  Copyright (c) 2015 SmartOSC. All rights reserved.
//

#import "NSMutableAttributedString+EWMultiStyle.h"

@implementation NSMutableAttributedString (EWMultiStyle)

+ (NSMutableAttributedString *)tb_multiStyleWithStrings:(NSArray *)arrStr withFonts:(NSArray *)arrFont
{
    NSString *string = [arrStr componentsJoinedByString:@""];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSInteger start = 0;
    
    for ( NSInteger i = 0; i < arrStr.count; i++ )
    {
        NSString *aStr = [arrStr objectAtIndex:i];
        UIFont *aFont = [arrFont objectAtIndex:i];
        [str addAttribute:NSFontAttributeName value:aFont range:NSMakeRange(start, aStr.length)];
        start = start + aStr.length;
    }
    
    return str;
}

+ (NSMutableAttributedString *)tb_multiStyleWithStrings:(NSArray *)arrStr withColors:(NSArray *)colors
{
    NSString *string = [arrStr componentsJoinedByString:@""];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSInteger start = 0;
    
    for ( NSInteger i = 0; i < arrStr.count; i ++ )
    {
        NSString *aStr = [arrStr objectAtIndex:i];
        UIColor *aColor = [colors objectAtIndex:i];
        [str addAttribute:NSBackgroundColorAttributeName value:aColor range:NSMakeRange(start, aStr.length)];
        start = start + aStr.length;
    }
    
    return str;
}

@end
