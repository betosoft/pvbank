//
//  NSMutableAttributedString+EWMultiStyle.h
//  mPos
//
//  Created by BunLV on 6/5/15.
//  Copyright (c) 2015 SmartOSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIFont+TBCore.h"

@interface NSMutableAttributedString (EWMultiStyle)

+ (NSMutableAttributedString *)tb_multiStyleWithStrings:(NSArray *)arrStr withFonts:(NSArray *)arrFont;
+ (NSMutableAttributedString *)tb_multiStyleWithStrings:(NSArray *)arrStr withColors:(NSArray *)colors;

@end
