//
//  QSAppDelegate.h
//  PVBank
//
//  Created by thuydd on 1/15/15.
//  Copyright (c) 2015 ThuyDao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
