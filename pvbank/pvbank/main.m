//
//  main.m
//  PVBank
//
//  Created by thuydd on 1/15/15.
//  Copyright (c) 2015 ThuyDao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TDAppDelegate class]));
    }
}
