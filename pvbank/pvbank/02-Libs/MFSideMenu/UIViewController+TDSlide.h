//
//  UIViewController+TDSlide.h
//  mPos
//
//  Created by Dao Duy Thuy on 5/27/15.
//  Copyright (c) 2015 ThuyDao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"

@interface UIViewController (TDSlide)
- (void)changeCenter:(UIViewController *)controller;

@end
