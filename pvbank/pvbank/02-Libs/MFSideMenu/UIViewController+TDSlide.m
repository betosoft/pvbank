//
//  UIViewController+TDSlide.m
//  mPos
//
//  Created by Dao Duy Thuy on 5/27/15.
//  Copyright (c) 2015 ThuyDao. All rights reserved.
//

#import "UIViewController+TDSlide.h"

@implementation UIViewController (TDSlide)

- (void)changeCenter:(UIViewController *)controller
{
    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
    NSArray *controllers = [NSArray arrayWithObject:controller];
    navigationController.viewControllers = controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}
@end
