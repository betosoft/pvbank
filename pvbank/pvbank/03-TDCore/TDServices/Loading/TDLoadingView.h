//
//  GMFLoadingView.h
//  testdropdownfilter
//
//  Created by Kaze on 4/3/15.
//  Copyright (c) 2015 Kaze. All rights reserved.
//

#import <UIKit/UIKit.h>

#define HIDELOADING(view,flag) [TDLoadingView hideAllInView:view animate:flag];
#define SHOWLOADING(view,flag) [TDLoadingView showInView:view animate:flag];

@interface TDLoadingView : UIView

+ (void)hideAllInView:(UIView *)containerView animate:(BOOL)animate;
+ (void)showInView:(UIView *)containerView animate:(BOOL)animate;

@end
