//
//  GMFLoadingView.m
//  testdropdownfilter
//
//  Created by Kaze on 4/3/15.
//  Copyright (c) 2015 Kaze. All rights reserved.
//

#import "TDLoadingView.h"
#import <objc/runtime.h>

static const char KTDLoadingView;

@interface TDLoadingView ()
@property (nonatomic, strong) UIImageView *staticView;
@property (nonatomic, strong) UIImageView *rotatingView;

@end

@implementation TDLoadingView

+ (void)showInView:(UIView *)containerView animate:(BOOL)animate
{
    TDLoadingView *loadingView = [[TDLoadingView alloc] init];
    NSMutableArray *a = objc_getAssociatedObject(containerView, &KTDLoadingView);
    if (a == nil)
    {
        a = [NSMutableArray new];
        objc_setAssociatedObject(containerView, &KTDLoadingView, a, OBJC_ASSOCIATION_RETAIN);
    }
    
    [a addObject:loadingView];
    
    loadingView.frame = containerView.bounds;
    if (animate == NO)
    {
        [containerView addSubview:loadingView];
        return;
    }
    
    loadingView.alpha = 0;
    [containerView addSubview:loadingView];
    
    [UIView animateWithDuration:0.2 animations:^
    {
        loadingView.alpha = 1;
    }];
}

+ (void)hideAllInView:(UIView *)containerView animate:(BOOL)animate
{
    NSMutableArray *a = objc_getAssociatedObject(containerView, &KTDLoadingView);
    if (a.count == 0)
        return;
    
    if (animate == NO)
    {
        for (UIView *v in a)
        {
            [v removeFromSuperview];
        }
        return;
    }
    
    [UIView animateWithDuration:0.3 animations:^
     {
         for (UIView *v in a)
         {
             CGFloat scale = 2;
             v.transform = CGAffineTransformScale(v.transform, scale, scale);
             v.alpha = 0;
         }
     }
     completion:^(BOOL finished)
     {
         for (UIView *v in a)
         {
             CGFloat scale = 2;
             v.transform = CGAffineTransformScale(v.transform, scale, scale);
             v.alpha = 0;
         }
     }];
}

- (void)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * duration];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGPoint centerPoint = CGPointMake(roundf(self.frame.size.width/2), round(self.frame.size.height/2));
    _rotatingView.center = centerPoint;
    _staticView.center = centerPoint;
}

- (void)createViews
{
    self.userInteractionEnabled = NO;
    
    _staticView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading-icon-static"]];
    _staticView.frame = CGRectMake(0, 0, 80, 80);
    _staticView.backgroundColor = [UIColor clearColor];
    _staticView.layer.cornerRadius = 40;
    _staticView.layer.masksToBounds = YES;
    _staticView.contentMode = UIViewContentModeCenter;
    _staticView.transform = CGAffineTransformScale(_staticView.transform, 0.25, 0.25);
    
    [self addSubview:_staticView];
    
    _rotatingView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading-icon-rotating"]];
    _rotatingView.frame = CGRectMake(0, 0, 80, 80);
    _rotatingView.backgroundColor = [UIColor clearColor];
    _rotatingView.contentMode = UIViewContentModeCenter;
    _rotatingView.transform = CGAffineTransformScale(_rotatingView.transform, 0.25, 0.25);
    
    [self addSubview:_rotatingView];
    
    [self runSpinAnimationOnView:_rotatingView duration:1];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        [self createViews];
    }
    return self;
}

@end
