

#import "TDServices.h"
#import "NSData+Base64.h"

@implementation TDServices

#pragma mark - Utils

+ (BOOL )checkConnection
{
    if ( ![TDReachability connectedToNetwork] )
    {
        //can't connect to internet
        return NO;
    }
    return YES;
}

+ (void)supportHTTPS:(AFHTTPRequestOperationManager *)manager
{
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy = securityPolicy;
}

#pragma mark - Private
+ (void)success:(id)responseObject entityClass:(Class)aClass completeAPIBlock:(CompleteAPIBlock)block
{
    if (responseObject == nil)
    {
        NSError *e = [NSError errorWithDomain:TD_DOMAIN code:1001 userInfo:nil];
        block (nil, e);
        return;
    }
    
    if (aClass == nil)
    {
        block(responseObject, nil);
        return;
    }
    
    id rslt = nil;
    NSError *error = nil;
    
    if ([aClass conformsToProtocol:@protocol(TDBaseResponseHandle)])
    {
        rslt = [((Class<TDBaseResponseHandle>)aClass) parseDataFromResponse:responseObject error:&error];
        block(rslt,error);
        return;
    }
    else
    {
        rslt = [aClass new];
        [rslt setValuesForKeysWithDictionary:responseObject];
    }
    
    block(responseObject, nil);
}

+ (void)failure:(NSError *)error completeAPIBlock:(CompleteAPIBlock)block
{
    block(nil,error);
}

#pragma mark - Call API
+ (void)callAPIWithUrl:(NSString *)url withTypeAPI:(TypeAPI)type withParams:(NSMutableDictionary *)dictParams entityClass:(Class)aClass completed:(CompleteAPIBlock)block
{
    // Check network
    if ( [self checkConnection] )
    {
        // Use AFHTTPRequestOperationManager
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@""]];
        
        //https support
        [self supportHTTPS:manager];
        
        // Request API
        //POST
        if (type == POST) {
            [manager POST:[TD_DOMAIN stringByAppendingString:url] parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 // Success
                 [self success:responseObject entityClass:aClass completeAPIBlock:block];
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 // Fails
                 [self failure:error completeAPIBlock:block];
             }];
            
        }
        else if (type == GET)
        {
            [manager GET:[TD_DOMAIN stringByAppendingString:url] parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 // Success
                 [self success:responseObject entityClass:aClass completeAPIBlock:block];
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 //fails
                 [self failure:error completeAPIBlock:block];
             }];
        }
        else if (type == DELETE)
        {
            [manager DELETE:url parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 // Success
                 [self success:responseObject entityClass:aClass completeAPIBlock:block];
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 //fails
                 [self failure:error completeAPIBlock:block];
             }];
        }
        else if (type == PUT)
        {
            [manager PUT:url parameters:dictParams success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 // Success
                 [self success:responseObject entityClass:aClass completeAPIBlock:block];
                 
             } failure:^(AFHTTPRequestOperation *operation, NSError *error)
             {
                 //fails
                 [self failure:error completeAPIBlock:block];
             }];
        }
    }
    else
    {
        //No internet connection
    }
}

#pragma mark - UPDATE AVATAR
+ (void)uploadAvatarBase64WithUrl:(NSString *)url  param:(NSMutableDictionary*)param   content:(UIImage*)contentImage entityClass:(Class)aClass completed:(CompleteAPIBlock)block {
    
    // Check network
    if ( [self checkConnection] )
    {
        //encode image to base 64 string
        NSData *data = UIImagePNGRepresentation(contentImage);
        NSString *str = [data base64EncodedString];
        [param setValue:str forKey:@"content"];
        
        //use AFHTTPRequestOperationManager
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        //request api
        [manager POST:[TD_DOMAIN stringByAppendingString:url] parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             //success
             [self success:responseObject entityClass:aClass completeAPIBlock:block];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             //fails
             [self failure:error completeAPIBlock:block];
         }];
    }
    else
    {
        
    }
}

+ (void)uploadAvatarWithURL:(NSString *)url withImage:(UIImage*)image param:(NSMutableDictionary*)param entityClass:(Class)aClass completed:(CompleteAPIBlock)block
{
    // Check network
    if ( [self checkConnection] )
    {
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@""]];
        
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        
        AFHTTPRequestOperation *op = [manager POST:[TD_DOMAIN stringByAppendingString:url]
                                        parameters:param
                         constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
                                      {
                                          
                                          [formData appendPartWithFileData:imageData name:@"avatar_file" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
                                          
                                      } success:^(AFHTTPRequestOperation *operation, id responseObject)
                                      {
                                          //success
                                          [self success:responseObject entityClass:aClass completeAPIBlock:block];
                                      }
                                           failure:^(AFHTTPRequestOperation *operation, NSError *error)
                                      {
                                          //fails
                                          [self failure:error completeAPIBlock:block];
                                      }];
        [op start];
    }
    else
    {
        
    }
}

@end

//==================================== Entity ======================================================

@implementation NSObject (TDServices)

//================== td_arrayFromJSONArray =============================

+ (NSMutableArray *)td_arrayFromJSONArray:(NSArray *)jsonArr;
{
    if (jsonArr == nil)
        return nil;
    NSMutableArray *a = [NSMutableArray new];
    for (NSDictionary *dic in jsonArr)
    {
        id obj = [self new];
        [obj setValuesForKeysWithDictionary:dic];
        [a addObject:obj];
    }
    return a;
}

//================ td_objectFromJSONObject =======================

+ (id)td_objectFromJSONObject:(NSDictionary *)jsonObj;
{
    id r = [self new];
    [r setValuesForKeysWithDictionary:jsonObj];
    return r;
}


//==================== td_arrayFromJSONObjet =====================

+ (NSMutableArray *)td_arrayFromJSONObjet:(NSDictionary *)jsonDict
{
    if (jsonDict == nil) {
        return nil;
    }
    NSMutableArray *a = [NSMutableArray new];
    for (NSString *key in [jsonDict allKeys]) {
        id obj = [self new];
        [obj setValuesForKeysWithDictionary:jsonDict[key]];
        [a addObject:obj];
    }
    return a;
}


@end
