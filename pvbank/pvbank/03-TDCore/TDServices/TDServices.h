

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "AFHTTPSessionManager.h"
#import "TDReachability.h"
#import "TDLoadingView.h"

#define TD_DOMAIN @"TB_DOMAIN"

#define CallAPI_WITHURL_WHITTYPEAPI_WITHPARAMS_WITHCOMPLETED(url,typeAPI,params,block) [TBServices callAPIWithUrl:url TypeAPI:typeAPI withParams:params completed:block]

@protocol TDBaseResponseHandle <NSObject>
@required

+ (id)parseDataFromResponse:(id)response error:(NSError **)error;

@end


typedef void(^CompleteAPIBlock)(id result, NSError *error);
typedef enum : NSUInteger {
    POST = 0,
    GET  = 1,
    PUT  = 2,
    DELETE = 3,
} TypeAPI;

@interface TDServices : NSObject

#pragma mark - Utils
+ (BOOL )checkConnection;
+ (void)supportHTTPS:(AFHTTPRequestOperationManager *)manager;

#pragma mark - Call API
+ (void)callAPIWithUrl:(NSString *)url withTypeAPI:(TypeAPI)type withParams:(NSMutableDictionary *)dictParams entityClass:(Class)aClass completed:(CompleteAPIBlock)block;

#pragma mark - UPDATE AVATAR
+ (void)uploadAvatarBase64WithUrl:(NSString *)url  param:(NSMutableDictionary*)param   content:(UIImage*)contentImage entityClass:(Class)aClass completed:(CompleteAPIBlock)block;

+ (void)uploadAvatarWithURL:(NSString *)url withImage:(UIImage*)image param:(NSMutableDictionary*)param entityClass:(Class)aClass completed:(CompleteAPIBlock)block;

@end

//==================================== Entity ======================================================

@interface NSObject (TDServices)

+ (NSMutableArray *)td_arrayFromJSONArray:(NSArray *)jsonArr;
+ (id)td_objectFromJSONObject:(NSDictionary *)jsonObj;
+ (NSMutableArray *)td_arrayFromJSONObjet:(NSDictionary *)jsonDict;

@end

