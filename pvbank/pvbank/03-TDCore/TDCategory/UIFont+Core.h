//
//  UIFont+Core.h
// Core
//
//  " on 8/16/14.
//  ". All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Core)

+ (UIFont *)fontWithSize:(CGFloat )size;
+ (UIFont *)boldFontWithSize:(CGFloat )size;
+ (UIFont *)italicFontWithSize:(CGFloat )size;
+ (UIFont *)lightFontWithSize:(CGFloat )size;
+ (UIFont *)regularFontWithSize:(CGFloat )size;

@end
