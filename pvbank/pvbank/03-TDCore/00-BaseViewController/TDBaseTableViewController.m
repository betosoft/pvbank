//
//  TBBaseTableViewController.m
//  mPos
//
//  Created by Dao Duy Thuy on 4/18/15.
//  Copyright (c) 2015 SmartOSCVietNam. All rights reserved.
//

#import "TDBaseTableViewController.h"

@interface TDBaseTableViewController ()

@end

@implementation TDBaseTableViewController

#pragma mark - lifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableView DataSource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseCell = [NSString stringWithFormat:@"%@cell",NSStringFromClass(self.cellClass)];
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:reuseCell];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:NSStringFromClass(self.cellClass) bundle:nil] forCellReuseIdentifier:reuseCell];
        cell = [tableView dequeueReusableCellWithIdentifier:reuseCell];
    }
    return cell.frame.size.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseCell = [NSString stringWithFormat:@"%@cell",NSStringFromClass(self.cellClass)];
    
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:reuseCell];
    if (!cell) {
        [tableView registerNib:[UINib nibWithNibName:NSStringFromClass(self.cellClass) bundle:nil] forCellReuseIdentifier:reuseCell];
        cell = [tableView dequeueReusableCellWithIdentifier:reuseCell];
    }
    SEL selector = @selector(fetchData:);
#warning "cell class need to config method fetchData"
    [cell performSelector:selector withObject:self.dataSource[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.callback) {
        self.callback(indexPath);
    }
}

@end
