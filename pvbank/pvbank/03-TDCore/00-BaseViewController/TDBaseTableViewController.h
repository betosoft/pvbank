//
//  TBBaseTableViewController.h
//  mPos
//
//  Created by Dao Duy Thuy on 4/18/15.
//  Copyright (c) 2015 SmartOSCVietNam. All rights reserved.
//

#import "TDBaseViewController.h"

typedef void (^td_callBackSelectedRow)(NSIndexPath *indexPath);
@interface TDBaseTableViewController : TDBaseViewController <UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, copy)td_callBackSelectedRow callback;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, copy) Class cellClass;
@property (nonatomic, weak) IBOutlet UITableView *tbList;

@end
